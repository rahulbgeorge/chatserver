from django.contrib import admin

# Register your models here.
from .models import Group

class Group(admin.ModelAdmin):
    list_display = ("id","title")