from django.db import models
from django.contrib.auth.models import User
# Create your models here.


class Session(models.Model):
    user = models.ForeignKey(User,on_delete=models.CASCADE)
    session_id = models.CharField(max_length=200,default=None,null=True)


class Group(models.Model):

    id = models.AutoField(primary_key=True)
    title = models.CharField(max_length=100)
    description = models.CharField(max_length=600,default=None,null=True)
    users = models.ManyToManyField(User,through="GroupUsers")

class GroupUsers(models.Model):

    user = models.ForeignKey(User,on_delete=models.CASCADE)
    group = models.ForeignKey(Group,on_delete=models.CASCADE)
    date_joined = models.DateTimeField(auto_now_add=True)
    is_admin = models.BooleanField(default=False)

class Message(models.Model):

    id = models.AutoField(primary_key=True)
    sender = models.ForeignKey(User,on_delete=models.CASCADE)
    message = models.TextField()
    group = models.ForeignKey(Group,on_delete=models.CASCADE)
    time = models.DateTimeField(auto_now_add=True)

