from django.http import HttpResponse
from django.shortcuts import render

# Create your views here.
from django.views.decorators.csrf import csrf_exempt


@csrf_exempt
def connect(request):
    print(request.META)
    print(request.POST)
    print(request.body)
    print(request.FILES)
    return HttpResponse("success")

@csrf_exempt
def message(request):
    print(request.META)
    print(request.POST)
    return HttpResponse("success")